package hackerrank.contests;

import java.util.Scanner;

public class CountingTriangles {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		
		int [] sides = new int[N];
		for (int i = 0; i < N; ++i){
			sides[i] = in.nextInt();
		}
		in.close();
		
		countTriangles(sides);
	}
	
	private static void countTriangles(int [] sides){
		int acuteCount = 0;
		int rightCount = 0;
		int obtuseCount = 0;
		for (int i = 0; i < sides.length - 3; ++i){
			
			int j = i + 1;
			
			int limit = 0;
			while (sides[j] < limit){
				int k = j + 1;
				
				while (sides[k] < limit){
					
					int type = typeOfTriangle(sides[i], sides[j], sides[k]);
					if (type == 1){
						acuteCount++;
					}
					else if (type == 2){
						rightCount++;
					}
					else if (type == 3){
						obtuseCount++;
					}
				}
			}
		
		}
		System.out.println(acuteCount + " " + rightCount + " " + obtuseCount);
	}
	
	//Returns 1 if triangle is 
	private static int typeOfTriangle(int a, int b, int c){
		
		return -1;
	}

}
