package hackerrank.contests;

import java.util.Scanner;

public class EmmasNotebook {

	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		int total = (n+1)*(n+2)/2 - 1;
		int ones = (total - n/2)/2 + n/2;
		System.out.println(ones);
		
		
	}
}
