package hackerrank.contests;

import java.util.Scanner;

public class SaveQuantumland {

	public static void main(String[] args) {
		
		/**
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		
		for (int i = 0; i < t; ++i){
			int n = in.nextInt();
			boolean [] guards = new boolean[n];
			for (int j = 0; j < n; ++j){
				int a = in.nextInt();
				if (a == 1){
					guards[j] = true;
				}
				else {
					guards[j] = false;
				}		
				

			}
			
			int newGuards = numberOfNewGuards(guards);
			System.out.println(newGuards);
		}
		**/
		testing();

	}
	
	private static void testing(){
		boolean [] guards = {false, false, true, false, false, false, false, false};
		System.out.println(numberOfNewGuards(guards));
	}
	private static int numberOfNewGuards(boolean [] guards){
		
		int neededGuards = 0;
		
		int start = 0; 
		int end = start;
		while (start < guards.length){
			
			//1. If i needs a guards
			if (needsAGuard(start, guards)){
			
				//Then find the last guy that needs a guard in that range.
				end = start + 1;
				while(end < guards.length && needsAGuard(end, guards)){
					end++;
				}
				end--;
				int range = end - start;
				neededGuards += Math.max(1, (range + range%2)/2);
				
				start = end + 1;
				
			}
			else{
				start++;
			}
		}
		
		return neededGuards;
	}

	private static boolean needsAGuard(int start, boolean [] guards){
		if (start == 0){
			if (!guards[start] && !guards[Math.max(start+1, guards.length-1)]){
				return true;
			}
		}
		else if (start == guards.length - 1){
			if (!guards[start] && !guards[Math.min(start - 1, 0)]){
				return true;
			}	
		}
		else{
			if (!guards[start-1] && !guards[start] && !guards[start+1]){
				return true;
			}
			return false;
		}
		
		return false;
		
	}
}
