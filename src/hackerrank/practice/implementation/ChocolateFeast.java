package hackerrank.practice.implementation;

import java.util.Scanner;

public class ChocolateFeast {

	public static void main(String[] args) {
		//1. Reading the input
		
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		int [] testCases = new int[T*3];
		for (int i = 0; i < 3*T; ++i){
			testCases[i] = in.nextInt();
			testCases[++i] = in.nextInt();
			testCases[++i] = in.nextInt();
		}
		in.close();
		
		
		//2. Performing calculations
		for (int i = 0; i < T; ++i){
			int N = testCases[i*3 + 0];
			int C = testCases[i*3 + 1];
			int M = testCases[i*3 + 2];
			
			int total = N/C;
			int wrappers = total;
			
			while (wrappers/M > 0){
				total += wrappers/M;
				int temp1 = wrappers/M;
				int temp2 = wrappers % M;
				wrappers = temp1 + temp2;
			}
			
			System.out.println(total);
		}

	}

}
