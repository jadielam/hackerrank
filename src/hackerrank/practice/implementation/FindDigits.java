package hackerrank.practice.implementation;

import java.util.Scanner;

public class FindDigits {

	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		
		long [] numbers = new long[T];
		for (int i = 0; i < T; ++i){
			int a = in.nextInt();
			numbers[i]=a;
		}
		in.close();
		
		for (int i = 0; i < numbers.length; ++i){
			System.out.println(numberOfDigits(numbers[i]));
		}
		
		
	}
	
	private static int numberOfDigits(long n){
		int counter = 0;
		long reducedN = n;
		while (reducedN > 0){
			long nextDigit = reducedN % 10;
			reducedN = reducedN/10;
			if (nextDigit != 0){
				if (n % nextDigit == 0) counter++;	
			}
			
		}
		
		return counter;
	}
}
