package hackerrank.practice.implementation;

import java.util.Scanner;

public class Encryption {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        
        //1. Remove spaces from the text.
        String noSpaces = s.replaceAll("\\s+","");
        
        //2. Write characters into a grid
        int length = noSpaces.length();
        double root = Math.sqrt(length);
        int floor = (int)Math.floor(root);
        int ceiling = (int)Math.ceil(root);
        int r, c;
        if (length == floor * floor) {
        	r = c = floor;
        }
        else if (length <= floor * ceiling) {
        	r = floor;
        	c = ceiling;
        }
        else {
        	r = c = ceiling;
        }
        
        char [][] charArray = new char[r][c];
        for (int i = 0; i < noSpaces.length(); ++i) {
        	char a = noSpaces.charAt(i);
        	int m = i / c;
        	int n = i % c;
        	charArray[m][n] = a;
        }
        
        //3. Display the encoded message.
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < c; j++) {
        	for (int i = 0; i < r; i++) {
        		char a = charArray[i][j];
        		if (a != 0){
        			sb.append(a);
        		}
        	}
        	sb.append(' ');
        }
        String finalResult = sb.toString();
        System.out.println(finalResult.trim());
    }
}
