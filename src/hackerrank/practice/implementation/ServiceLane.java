package hackerrank.practice.implementation;

import java.util.Scanner;

public class ServiceLane {

	public static void main(String [] args){
		
		//1. Reading input
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int T = in.nextInt();
		int [] width = new int[N];
		for (int i = 0; i < N; ++i){
			width[i] = in.nextInt();
		}
		int [] testCases = new int[T*2];
		for (int i = 0; i < T*2; ++i){
			testCases[i] = in.nextInt();
			testCases[++i] = in.nextInt();
		}
		in.close();
		
		//2. Calculating
		for (int i = 0; i < testCases.length; ++i){
			System.out.println(maxVehicle(N, width, testCases[i], testCases[++i]));
		}
		
	}
	
	/**
	 * Returns the max vehicle size that can pass trough the service lane
	 * @param highwaySize
	 * @param widthArray
	 * @param i
	 * @param j
	 * @return
	 */
	private static int maxVehicle(int highwaySize, int[] widthArray, int i, int j){
		int min = 3;
		for (int a = i; a <= j; ++a){
			if (widthArray[a]< min){
				min = widthArray[a];
			}
		}
		return min;
	}
}
