package hackerrank.practice.implementation;

import java.util.Scanner;

public class AngryProfessor {

	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for (int i = 0; i < T; ++i){
			int N = in.nextInt();
			int K = in.nextInt();
			
			int kCounter = 0;
			for (int j = 0; j < N; ++j){
				int arrivalTime = in.nextInt();
				if (arrivalTime <= 0) kCounter++;
			}
			
			//The class gets cancelled
			if (kCounter < K){
				System.out.println("YES");
			}
			
			//The class does not get cancelled
			else{
				System.out.println("NO");
			}
		}
		
		in.close();
	}
}
