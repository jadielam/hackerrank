package hackerrank.practice.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ManasaAndStones {

	public static void main(String [] args){
		
		//Reading input
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for (int i = 0; i < T; ++i){
			int n = in.nextInt();
			int a = in.nextInt();
			int b = in.nextInt();
			
			List<Integer> lastStones = finalStones(n, a, b);
			StringBuilder sb = new StringBuilder();
			for (Integer last : lastStones){
				sb.append(last);
				sb.append(" ");
			}
			sb.delete(sb.length()-1, sb.length());
			System.out.println(sb.toString());
		}
		in.close();
	}
	
	
	private static List<Integer> finalStones(int n, int a, int b){
		
		Set<Integer> previousSteps = new HashSet<Integer>();
		previousSteps.add(0);
		
		for (int i = 1; i < n; ++i){
			HashSet<Integer> nextSteps = new HashSet<Integer>();
			for (Integer previousStep : previousSteps){
				nextSteps.add(previousStep + a);
				nextSteps.add(previousStep + b);
				previousSteps = nextSteps;
			}
		}
		List<Integer> toReturn = new ArrayList<Integer>(previousSteps);
		Collections.sort(toReturn);
		return toReturn;
	}
}
