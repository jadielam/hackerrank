package hackerrank.practice.implementation;

import java.util.Scanner;
import java.util.BitSet;

public class ACMICPCTeam {

	public static void main(String[] args) {
		
		//1. GEtting the input
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		
		BitSet [] bitSetArray = new BitSet[N];
		for (int i = 0; i < N; ++i){
			String bits = in.next();
			bitSetArray[i] = parseToBitSet(bits);
		}
		in.close();
		
		//1. Solving the problem
		int maximum = 0;
		int numberOfTeams = 0;
		
		for (int i = 0; i < bitSetArray.length; ++i){
			for (int j = i+1; j < bitSetArray.length; ++j){
				int numberOfTopics = getNumberOfTopics(bitSetArray[i], bitSetArray[j]);
				if (numberOfTopics > maximum){
					maximum = numberOfTopics;
					numberOfTeams = 1;
				}
				else{
					if (numberOfTopics == maximum){
						numberOfTeams++;
					}
				}
			}
		}

        System.out.println(maximum);
        System.out.println(numberOfTeams);
		
	}

	private static BitSet parseToBitSet(String bits) {
		BitSet toReturn = new BitSet(bits.length());
		for (int i = 0; i < bits.length(); ++i) {
			if (bits.charAt(i) == '1') {
				toReturn.set(i);
			}
		}
		return toReturn;
	}
	
	private static int getNumberOfTopics(BitSet a, BitSet b) {
		
		BitSet temp = BitSet.valueOf(a.toByteArray());
		temp.or(b);	
		return temp.cardinality();
	}
}
