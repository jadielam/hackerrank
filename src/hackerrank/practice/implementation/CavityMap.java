package hackerrank.practice.implementation;

import java.util.Scanner;

public class CavityMap {

	public static void main(String[] args) {
		//1. Reading input
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		
		int [][]map = new int[n][n];
		
		for (int i = 0; i < n; i++){
			String line = in.next();
			
			
			if (line.length() >=n){
				for (int j = 0; j < n; ++j){
					map[i][j] = Integer.parseInt(line.substring(j, j+1));
				}	
			}
			
		}
		in.close();
		
		//2. Making calculations
		
		for (int i = 1; i < (n-1); ++i){
			for (int j = 1; j <(n-1); ++j){
				int interiorPoint = map[i][j];
				if (interiorPoint > map[i-1][j]
						&& interiorPoint > map[i][j-1]
						&& interiorPoint > map[i+1][j]
						&& interiorPoint > map[i][j+1]
						
						){
				
					map[i][j] = 10;
					
				}
			}
		}
		
		for (int i = 0; i < n; ++i){
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < n; ++j){
				if (map[i][j]!= 10) sb.append(map[i][j]);
				else sb.append("X");
			}
			System.out.println(sb.toString());
		}

	}

}
