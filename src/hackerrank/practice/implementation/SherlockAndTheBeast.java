package hackerrank.practice.implementation;

import java.util.Scanner;

public class SherlockAndTheBeast {

	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		int [] digits = new int[T];
		
		for (int i = 0; i < T; ++i){
			digits[i] = in.nextInt();
		}
		in.close();
		
		
		for (int i = 0; i < digits.length; ++i){
			System.out.println(maxDecentNumber(digits[i]));
		}
		
		
	}
	
	//Returns the maximum decent number with the given number of digits.
	//Returns -1 if such number does not exist
	private static String maxDecentNumber(int digits){
		int i = 0;
		int digits_3 = 0; //contains the number of digits that will have 3's
		int digits_5 = 0; //contains the number of digits that will have 5's
		
		do {
			digits_3 = 5*i;
			if (digits_3 > digits) break;
			digits_5 = digits - digits_3;
			++i;

		}
		while ((digits_5 % 3) !=0 || (digits_3 % 5) !=0);
		
		if (digits_3 > digits) return "-1";
		
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < digits_5; ++j){
			sb.append("5");
		}
		for (int j = 0; j < digits_3; ++j){
			sb.append("3");
		}
		return sb.toString();
	}
}
