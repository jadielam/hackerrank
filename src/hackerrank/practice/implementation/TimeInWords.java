package hackerrank.practice.implementation;

import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
public class TimeInWords {

	private static Map<Integer, String> numbers;
	
	static {
		numbers = new HashMap<Integer, String>();
		numbers.put(1, "one");
		numbers.put(2, "two");
		numbers.put(3, "three");
		numbers.put(4, "four");
		numbers.put(5, "five");
		numbers.put(6, "six");
		numbers.put(7, "seven");
		numbers.put(8, "eight");
		numbers.put(9, "nine");
		numbers.put(10, "ten");
		numbers.put(11, "eleven");
		numbers.put(12, "twelve");
		numbers.put(13, "thirteen");
		numbers.put(14, "fourteen");
		numbers.put(15, "quarter");
		numbers.put(16, "sixteen");
		numbers.put(17, "seventeen");
		numbers.put(18, "eighteen");
		numbers.put(19, "nineteen");
		numbers.put(20, "twenty");
		numbers.put(21, "twenty one");
		numbers.put(22, "twenty two");
		numbers.put(23, "twenty three");
		numbers.put(24, "twenty four");
		numbers.put(25, "twenty five");
		numbers.put(26, "twenty six");
		numbers.put(27, "twenty seven");
		numbers.put(28, "twenty eight");
		numbers.put(29, "twenty nine");
		numbers.put(30, "half");
		
	}
	
	public static void main(String [] args) {
        Scanner in = new Scanner(System.in);
        int h = in.nextInt();
        int m = in.nextInt();
        int actualM = -1;
        
        //1. Preparing the String values
        if (m > 30) {
        	h = h + 1;
        	actualM = 60 - m;
        }
        else {
        	actualM = m;
        }
        
        String hour = numbers.get(h);
        String minutes = numbers.get(actualM);
        String minuteFiller = " minutes";
        if (actualM == 1) {
        	minuteFiller = " minute";
        }
        if (actualM == 15 || actualM == 30) {
        	minuteFiller = "";
        }
        
        
        if (m == 0) {
        	System.out.println(hour + " o' clock");
        } else {
        	if (m > 30) {
        		System.out.println(minutes + minuteFiller + " to " + hour);
        	}
        	else {
        		System.out.println(minutes + minuteFiller + " past " + hour);
        	}
        }     
	}
	
	
}
