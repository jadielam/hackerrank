package hackerrank.practice.implementation;

import java.util.Scanner;

public class CaesarCipher {

	public static void main(String[] args) {
		//1. Reading input
		Scanner in = new Scanner(System.in);
		int L = in.nextInt();
		char [] text = new char[L];
		String textS = in.next();
		for (int i = 0; i < L; ++i){
			text[i] = textS.charAt(i);
		}

		int K = in.nextInt();
		in.close();
		
		
		//2. Making calculations
		for (int i = 0; i < text.length; ++i){
			
			if (text[i] >= 'a' && text[i] <='z'){
				text[i] = (char) ('a' + (char)((text[i] - 'a' + K) % ('z' - 'a' + 1)));
			}
			else if (text[i] >= 'A' && text[i] <='Z'){
				text[i] = (char) ('A' + (char)((text[i] -'A' + K) % ('Z' - 'A' + 1 )));
			}
		}
		
		//3. Printing the result
		for (int i = 0; i < text.length; ++i){
			System.out.print(text[i]);
		}
		

	}

}
