package hackerrank.practice.implementation;

import java.util.Arrays;
import java.util.Scanner;

public class CutTheSticks {

	public static void main(String[] args) {
		//1. Reading the input
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int [] sticks = new int[N];
		
		for (int i = 0; i < N; ++i){
			sticks[i] = in.nextInt();
		}
		in.close();
		
		//2. Calculating solution
		Arrays.sort(sticks);
		int end = 0;
		int start = 0;
		int stickSize = 0;
		while (end < sticks.length){
			
			int tempSizeAddition = sticks[start] - stickSize;
			stickSize += tempSizeAddition;
			while (end < sticks.length && sticks[end] <=stickSize){
				end++;
			}
			
			System.out.println(sticks.length - start);
			start = end;
		}
	}

}
