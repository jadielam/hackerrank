package hackerrank.practice.implementation;

import java.util.Scanner;

public class SherlockAndSquares {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		long [] numbers = new long[T*2];
		
		for (int i = 0; i < T*2; ++i){
			long a = in.nextLong();
			numbers[i] = a;
		}
		in.close();
		
		for (int i = 0; i < numbers.length; ++i){
			long A = numbers[i];
			long B = numbers[++i];
			
			long count = fasterGetCount(A, B);
			System.out.println(count);
		}

	}
	
	/**
	 * Returns the count of square integers between integers A and B
	 * inclusive.  An square integer S is an integer such that S = C*C, where
	 * C is an integer.
	 * 
	 * This method is too slow to pass the hackerrank tests.  So, use another
	 * implementation below.
	 * @param A
	 * @param B
	 * @return
	 */
	private static long slowGetCount(long A, long B){
		
		long count = 0;
		
		for (long i = A; i <= B; ++i){
			long sqrt = (long)Math.sqrt(i);
			if (sqrt * sqrt == i){
				count++;
			}
		}
		return count;
	}
	
	/**
	 * This function returns the same result as the previous functions, only
	 * that it works faster.  THe trick is the following:
	 * 
	 * When working in hexadecimal, perfect squares can only end in 0, 1, 4, or 9 (base 16)
	 * @param A
	 * @param B
	 * @return
	 */
	private static long fastGetCount(long A, long B){
		long count = 0;
		
		for (long i = A; i <= B; ++i){
			if (isPerfectSquare(i)){
				count++;
			}
		}
		
		return count;
	}

	/**
	 * 
	 * This function computes the same value as the previous one, but it
	 * skips values by instead just finding the first integer in the sequence
	 * that is a square integer, and from them on, never calling Math.sqrt again
	 * by just simply incrementing the sqrt and multiplying it by itself to check
	 * if the result is still less than or equal to B
	 * @param A
	 * @param B
	 * @return
	 */
	private static long fasterGetCount(long A, long B){
		
		long count = 0;
		
		long i = A;
		for (; i <= B; ++i){
			if (isPerfectSquare(i)){
				break;
			}
		}
        if (i > B) return 0;
        
		long root = (long)Math.sqrt(i);
		while (root * root <= B){
			count++;
			root++;
		}
		
		return count;
	}
	
	private static boolean isPerfectSquare(long n){
		switch((int) (n & 0xF)){
			case 0: case 1: case 4: case 9:
				long tst = (long)Math.sqrt(n);
				if (tst * tst == n){
					return true;
				}
			
			default: return false;
		}

	}
}
