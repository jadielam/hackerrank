package hackerrank.practice.implementation;

import java.util.Scanner;
import java.io.FileInputStream;
import java.math.BigInteger;

public class TaumAndBDay {

	public static void main (String [] args) {
	
		/**
		try{
			System.setIn(new FileInputStream("input.txt"));
		}
		catch(Exception e){
			e.printStackTrace();
			return;
		}
		**/
		
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int b = in.nextInt();
            int w = in.nextInt();
            int x = in.nextInt();
            int y = in.nextInt();
            int z = in.nextInt();
         
            BigInteger bBig = BigInteger.valueOf(b);
            BigInteger wBig = BigInteger.valueOf(w);
            BigInteger xBig = BigInteger.valueOf(x);
            BigInteger yBig = BigInteger.valueOf(y);
            BigInteger zBig = BigInteger.valueOf(z);
            
            BigInteger cost = BigInteger.valueOf(0);
            cost = (bBig.multiply(xBig)).add(wBig.multiply(yBig));
            
            int maxCost = Math.max(x, y);
            int minCost = Math.min(x, y);
            
            //If conversion makes sense
            if (maxCost > (z + minCost)) {
            	if (x > minCost) {
            		cost = (bBig.multiply(yBig)).add(wBig.multiply(yBig)).add(bBig.multiply(zBig));;
            	}
            	else if (y > minCost) {
            		cost = (bBig.multiply(xBig)).add(wBig.multiply(xBig)).add(wBig.multiply(zBig));
            	}
            }
            System.out.println(cost);
        }
        in.close();
	}
	
}
