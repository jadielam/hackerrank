package hackerrank.practice.implementation;

import java.util.Scanner;

public class TheGridSearch {

    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        
        //1. Read the number of tests
        int T = in.nextInt();
        
        for (int i = 0; i < T; ++i){
            
            //2. Read the dimensions of the first matrix
            int m = in.nextInt();
            int n = in.nextInt();
            
            //3. Read the first matrix
            in.nextLine();
            int [][] matrix = new int[m][n];
            for (int j = 0; j < m; ++j){
                String line = in.nextLine();
                line = line.trim();
                for (int k = 0; k < n; ++k){
                    
                    matrix[j][k] = Character.getNumericValue(line.charAt(k));
                }
            }
        
            //4. Read the dimensions of the second matrix
            int m1 = in.nextInt();
            int n1 = in.nextInt();
            int [][] pattern = new int[m1][n1];
            
            //5. Read the second matrix
            in.nextLine();
            for (int j = 0; j < m1; ++j){
                String line = in.nextLine();
                for (int k = 0; k < n1; ++k){
                    pattern[j][k] = Character.getNumericValue(line.charAt(k));
                }
            }
            System.out.println(i);
            if (i == 3){
                //6. Find the second matrix inside the first one.
                //6.1
                boolean patternFound = false;
                for (int j = 0; j < m - m1; ++j){
                    for (int k = 0; k < (n - n1); ++k){
                        
                        //Check for the submatrix beginning here with the same dimensions as matrix
                        // pattern
                        boolean errorFound = false;
                        for(int w = j; w < (j + m1); ++w){
                            for (int h = k; h < (k + n1); ++h){
                                
                                if (matrix[w][h] == pattern[w-j][h-k]){
                                    if (w == (j + m1 - 1) && h == (k + n1 -1 )){
                                        patternFound = true;
                                        break;
                                    }
                                    
                                }
                                else{
                                    errorFound = true;
                                }
                                if (errorFound) break;
                                else if (patternFound) break;
                            }
                            if (errorFound) break;
                            else if (patternFound) break;
                        }
                        if (patternFound) break;
                    }
                    if (patternFound) break;
                }
                if (patternFound){
                    System.out.println("YES");    
                }
                else{
                    System.out.println("NO");
                }

            }
            
        }
        
        in.close();
    }

}
