package hackerrank.practice.warmup;


import java.util.*;

public class AVeryBigSum {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        
        long sum = 0;
        for (int i = 0; i < N; ++i){
            sum+=in.nextInt();
        }
        System.out.println(sum);
        in.close();
    }
}