package hackerrank.practice.warmup;

import java.util.*;

public class DiagonalDifference {

    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        int N;
        N = in.nextInt();
        long diagonal1Sum = 0;
        long diagonal2Sum = 0;
        
        for (int i = 0; i < N; ++i){
            for (int j = 0; j < N; ++j){
                
                int temp = in.nextInt();
                
                if (i == j){
                    diagonal1Sum  += temp; 
                }
                if (j == (N - i - 1)){
                    diagonal2Sum += temp;
                }
            }
        }
        
        System.out.println(Math.abs(diagonal1Sum - diagonal2Sum));
        in.close();
        
        
    }
}