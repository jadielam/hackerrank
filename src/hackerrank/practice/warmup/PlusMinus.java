package hackerrank.practice.warmup;

import java.util.*;
import java.text.*;


public class PlusMinus {

    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int positiveCount = 0;
        int negativeCount = 0;
        int zeroCount = 0;
        
        for (int i = 0; i < N; ++i){
            int a = in.nextInt();
            if (a < 0) {
                negativeCount++;
            }
            else if (a > 0) {
                positiveCount++;
            }
            else {
                zeroCount++;
            } 
        }
        
        in.close();
        DecimalFormat df = new DecimalFormat("#.000");
        System.out.println(df.format(((float)positiveCount)/N));
        System.out.println(df.format(((float)negativeCount)/N));
        System.out.println(df.format(((float)zeroCount)/N));
        
        
        
    }
}