package hackerrank.practice.warmup;

import java.util.Scanner;

public class LibraryFine {

	public static void main(String []args){
		Scanner in = new Scanner(System.in);
		int aDay = in.nextInt();
		int aMonth = in.nextInt();
		int aYear = in.nextInt();
		
		int eDay = in.nextInt();
		int eMonth = in.nextInt();
		int eYear = in.nextInt();
		
		in.close();
		//1. If the book is returned on or before the expected return date.
		if (isBefore(aDay, aMonth, aYear, eDay, eMonth, eYear)){
			System.out.println(0);
			return;
		}
		
		//2. If the book is returned in the same calendar month
		if (aYear == eYear && aMonth == eMonth){
			System.out.println(15 * (aDay - eDay));
			return;
		}
		
		//3. If the book is not returned in the same calendar month, but in the
		//same calendar year
		if (aYear == eYear){
			System.out.println(500 * (aMonth - eMonth));
			return;
		}
		
		//4. If the book is not returned in the same calendar year
		System.out.println("10000");
	}
	
	//Returns true if the actual date is less than or equal to the
	//expected date.
	private static boolean isBefore(int aDay, int aMonth, int aYear,
			int eDay, int eMonth, int eYear){
		
		if (aYear < eYear) return true;
		else if (aYear == eYear && aMonth < eMonth) return true;
		else if (aYear == eYear && aMonth == eMonth && aDay <= eDay) return true;
		
		return false;
	}
}
