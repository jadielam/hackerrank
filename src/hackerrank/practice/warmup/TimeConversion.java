package hackerrank.practice.warmup;

import java.util.Scanner;

public class TimeConversion {

	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		in.close();
		String line = in.nextLine();
		
		
		//1. Parsing the hours
		String hourString = line.substring(0, 2);
		String minuteString = line.substring(3, 5);
		String secondString = line.substring(6, 8);
		try{
			String amPmString = line.substring(8, 10);
			if (amPmString.equals("AM")){
				int hour = Integer.parseInt(hourString);
				if (hour == 12) hourString = "00";
				StringBuilder sb = new StringBuilder();
				sb.append(hourString).append(line.substring(2, 8));
				
				System.out.println(sb.toString());
			}
			else{
				int hour = Integer.parseInt(hourString);
				if (hour != 12) hour += 12;
				StringBuilder sb = new StringBuilder();
				sb.append(hour).append(":").append(minuteString).append(":").append(secondString);
				System.out.print(sb.toString());
				
			}
			
			
			
			
		}
		//This is midnight
		catch(IndexOutOfBoundsException e){
			System.out.println("00:00:00");
		}
		
	
	}
}
