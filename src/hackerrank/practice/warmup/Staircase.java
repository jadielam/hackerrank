package hackerrank.practice.warmup;

import java.util.Scanner;

public class Staircase {

	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		in.close();
		
		for (int i = 0; i < N; ++i){
			int j = 0;
			for (; j < N - (i+1); j++){
				System.out.print(" ");
				
			}
			for (; j < N; ++j){
				System.out.print("#");
			}
			System.out.println();
		}
	}
}
