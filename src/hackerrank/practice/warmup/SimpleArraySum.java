package hackerrank.practice.warmup;


import java.util.*;

public class SimpleArraySum {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N;
        N = in.nextInt();
        int sum = 0;
        for (int i = 0; i < N; ++i){
            sum+=in.nextInt();
        }
        in.close();
        System.out.println(sum);
    }
}